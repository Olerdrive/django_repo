from django.db import models
from django.contrib.auth.models import User
from django.db.models import Count
from datetime import datetime

class QuestionManager(models.Manager):
	def hot(self):
		questions = Question.objects.order_by('-rating')
		for obj in questions:
			obj = Question.get_Question_Info(obj)
		return questions
	
	def all(self):
		questions = Question.objects.order_by('-date')
		for obj in questions:
			obj = Question.get_Question_Info(obj)
		return questions
	
	def tag(self, tag_text):
		tag_obj  =  Tag.objects.get(name=tag_text)
	#	tagged_questions = tag_obj.question.all()	
		return	tag_obj.question.filter(tag=tag_obj).order_by('-date') 
        
class Profile(models.Model):
	avatar = models.FileField(upload_to='uploads/', blank=False)
        user = models.OneToOneField(User, blank=False, null=False)

	def __unicode__(self):
		return self.user.username


class Question(models.Model):
	title = models.CharField(max_length=255)
	text = models.TextField(max_length=1000)
	rating = models.IntegerField()
	date = models.DateField(default=datetime.now)
	author = models.ForeignKey(Profile)
	objects = QuestionManager()
	def __unicode__(self):
		return 'ID: ' + str(self.id) + ' Title: ' + self.title

	def get_Question_Info(question):
	        question.tags = Question.get_Tags_By_ID(question.id)
                question.answer_count = Question.get_Answer_Count(question.id)
                return question

	@staticmethod
        def get_Answer_Count(question_id):
                return Answer.objects.filter(question__pk=question_id).count()

	@staticmethod	
        def get_Question_By_ID(question_id):
                question = Question.objects.get(pk=question_id)
                return question
	
	@staticmethod
        def get_Tags_By_ID(question_id):
                tags = Tag.objects.filter(question__pk=question_id)
                return tags
	
	@staticmethod
        def get_Answers_By_ID(question_id):
                answer_count = Answer.objects.filter(question__pk=question_id)
                answers = answer_count.order_by('-date')
                return answers

	@staticmethod
        def get_Answer_Count(question_id):
                return Answer.objects.filter(question__pk=question_id).count()

class Answer(models.Model):
        text = models.TextField(max_length=1000)
        date = models.DateField(default=datetime.now)
	question = models.ForeignKey(Question)
        author = models.ForeignKey(Profile)
        is_correct = models.BooleanField()
	def __unicode__(self):
		return 'ID: ' + str(self.id) + ' Text: ' + self.text + ' for Question ID: ' + str(self.question.id) + ' ' + self.question.title


class Tag(models.Model):
        name = models.CharField(max_length=50)
	question = models.ManyToManyField(Question)
	#answer = models.ManyToManyField(Answer)
        def __unicode__(self):
                return  self.name


class Q_Like(models.Model):
	user = models.ForeignKey(Profile)
	question = models.ForeignKey(Question)


class A_Like(models.Model):
	user = models.ForeignKey(Profile)
	answer = models.ForeignKey(Answer)
# Create your models here.
