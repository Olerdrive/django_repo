from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import auth
from ask.models import Question, Answer
from ask.forms import *

def login(request):
  if request.POST:
	my_username = request.POST.get('login')
	my_password = request.POST.get('password')
	user = auth.authenticate(username=my_username, password=my_password)
        
	redirect_path = request.GET.get('next_to')	
	if not redirect_path:
		redirect_path = '/'
	
	if user is not None:
		auth.login(request, user)
		return redirect(redirect_path)
  else:
	return render(request, 'ask/login.html')

@login_required
def logout(request):
	auth.logout(request)
	redirect_path = request.REQUEST.get('next_to', '')
        if not redirect_path:
                redirect_path = '/'	

	return redirect(redirect_path)
	
def signup(request):
	if request.POST:
		form = RegForm(request.POST, request.FILES)
		if form.is_valid():
			form.save_user(request)
			return redirect('/')
		return render(request, 'ask/signup.html', { 'form': form })
	else:
		form = RegForm()
		return render(request, 'ask/signup.html', { 'form': form })

def paginate(object_list, objects_count, page_num):

	paginator = Paginator(object_list, objects_count)
	try:
		objects_page = paginator.page(page_num)
	except EmptyPage:
        	objects_page = paginator.page(paginator.num_pages)
	return objects_page

def index(request, page = 1):
	questions = Question.objects.all()
	questions = paginate(questions, 5, page)
	return render(request, 'ask/index.html', {
		'objects': questions,
		'view_name': 'index',
		'url_prefix': '',
	})

def hot_index(request, page = 1):
        questions = Question.objects.hot()
        questions = paginate(questions, 5, page) 
	return render(request, 'ask/hot.html', {
                'objects': questions,
		'view_name': 'hot_index',
		'url_prefix': '/hot'
        })


@login_required(redirect_field_name='next_to')
def ask(request):
	if request.POST:
                form = AskForm(request.POST)
		if form.is_valid():
                	form.save_question(request)
                        return redirect('/')
		return render(request, 'ask/ask.html',{'form': form})	
	else: 
		form = AskForm()
        	return render(request, 'ask/ask.html',{
			'form': form,
			})

def answers(request, id, page=1):
	question = Question.get_Question_By_ID(id)
        answers = paginate(Question.get_Answers_By_ID(id), 5, page)
	if request.POST:
		form = AnswerForm(request.POST)	

		if form.is_valid():
			form.save_answer(request, question)
			return redirect('/')	
	else:
		form = AnswerForm()
		
	return render(request, 'ask/answers.html', {
		'question': question,
		'objects': answers,
		'answer_count': Question.get_Answer_Count(id),
		'tags': Question.get_Tags_By_ID(id),
		'view_name': 'answers',
		'url_prefix': '/question/{}'.format(id),
		'current_path': request.get_full_path(),
		'form': form,
		})
	

def tag_index(request, tag, page=1):
        questions = Question.objects.tag(tag)
        for obj in questions:
                obj = Question.get_Question_Info(obj)	

        questions = paginate(questions, 5, page)

        return render(request, 'ask/tag_index.html', {
                'objects': questions,
                'view_name': 'tag_index',
		'current_tag': tag,
		'url_prefix': '/tag/{}'.format(tag)
        })


# Create your views here.
