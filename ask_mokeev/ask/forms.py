from django import forms
from ask.models import *
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate

class AnswerForm(forms.Form):
	text = forms.CharField(widget=forms.Textarea(attrs=
                                {'class': 'form-control input-md',
                                 'required': '',

                                }),
                                label="Your Answer")

	def save_answer(self, request, q):
		text = self.cleaned_data.get('text')
		answer = Answer.objects.create(text=text,
					       author = request.user.profile,
					       date = datetime.today(),
					       question = q,
					       is_correct = 0,
						)
		answer.save()

class AskForm(forms.Form):
	title = forms.CharField(widget=forms.TextInput(attrs=
				{'class': 'form-control input-md',
			 	 'placeholder': 'Name your question',
			 	 'required': '',
				}),
				
				label="Title")
	text = forms.CharField(widget=forms.Textarea(attrs=
				{'class': 'form-control input-md',
                                 'placeholder': 'Ask everything you want',
                                 'required': '',   

				}),
				label="Text")

	tags = forms.CharField(widget=forms.TextInput(attrs=
                                {'class': 'form-control input-md',
                                 'placeholder': 'Name your question',
                                 'required': '',
                                }),

                                label="Tags")

	def save_question(self, request):
		title = self.cleaned_data.get('title')
		text = self.cleaned_data.get('text')
		tags = self.cleaned_data.get('tags').encode('utf8').split(', ')
		q = Question.objects.create(title=title, 
					    text=text, 
					    author=request.user.profile,
					    date=datetime.today(),
					    rating=0,)
		for tag in tags:
			tag = Tag.objects.create(name=tag)
			q.tag_set.add(tag)

		q.save()

class RegForm(forms.Form):
	username    = forms.CharField(widget=forms.TextInput(attrs=
				{'class': 'form-control input-md', 
				 'placeholder': 'Your nickname',
				 'required': ' ',
								}))
	email       = forms.EmailField(widget=forms.EmailInput(attrs=
				{'class': 'form-control input-md',
				'placeholder': 'example@domain.com',
				'required': ' ',
								}))
	password    = forms.CharField(widget=forms.PasswordInput(attrs=
				{'class': 'form-control input-md',
				'required': ' ',
								}))
	password2 = forms.CharField(widget=forms.PasswordInput(attrs=
				{'class': 'form-control input-md',
				'required': ' ',
								}),
				label = "Repeat your password"	 )

	avatar = forms.ImageField()

	def save_user(self, request):
		username = self.cleaned_data.get('username')
		email = self.cleaned_data.get('email')
		password = self.cleaned_data.get('password')
		avatar = self.cleaned_data.get('avatar')
		user = User.objects.create_user(username, email, password)
		user.save()

		user = authenticate(username = username, password = password)
		login(request, user)

		profile = Profile()
		profile.user = user
		profile.avatar = avatar
		profile.save()
		

	def clean_password(self):
        	if self.data['password'] != self.data['password2']:
            		raise forms.ValidationError('Passwords are not the same')
        	return self.data['password']


    	def clean(self,*args, **kwargs):
        	self.clean_password()
        	return super(RegForm, self).clean(*args, **kwargs)
