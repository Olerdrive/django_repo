from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    # url(r'^$', 'ask_mokeev.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^admin/', include(admin.site.urls)),
    url(r'^hello/', 'ask_mokeev.views.hello', name='hello'),
    url(r'^hot/(?P<page>\d+)/$', 'ask.views.hot_index', name='hot_index'),
    url(r'^hot/$', 'ask.views.hot_index', name='hot_index'),

    url(r'^signup/$', 'ask.views.signup', name='signup'),
    url(r'^login/$', 'ask.views.login', name='login'),
    url(r'^ask/$', 'ask.views.ask', name='ask'),

    url(r'^question/(?P<id>\d+)/(?P<page>\d+)/$', 'ask.views.answers', name='answers'),
    url(r'^question/(?P<id>\d+)/$', 'ask.views.answers', name='answers'),

    url(r'^tag/(?P<tag>\w+)/(?P<page>\d+)/$', 'ask.views.tag_index', name='tag_index'),
    url(r'^tag/(?P<tag>\w+)/$', 'ask.views.tag_index', name='tag_index'),


    url(r'^(?P<page>\d+)/$', 'ask.views.index', name='index'),
    url(r'^$', 'ask.views.index', name='index'),
]
